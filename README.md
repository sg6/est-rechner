# EstRechner

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.5.0.

## Purpose

The purpose of this project is to give an overview of legal payments for self-employed Austrians.
It should give a quick and rough amount of payments for social insurance and taxes.

## Status

The project is still in progress and not finished yet.
Please keep in mind, this is not and will never be a legally binding calculator. 
It should only give an overview; errors and new laws can cause an extreme difference to the calculations used in this project.